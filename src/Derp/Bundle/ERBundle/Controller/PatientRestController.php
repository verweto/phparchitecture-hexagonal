<?php

namespace Derp\Bundle\ERBundle\Controller;

use Derp\Command\RegisterWalkIn;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/api/patients/")
 */
class PatientRestController extends Controller
{

    /**
     * @Route("/walk-ins")
     * @Method("post")
     */
    public function registerAction(Request $request)
    {
        $patientId = $this->get('patient_repository')->nextIdentity();
        $command = $this->get('jms_serializer')
            ->deserialize($request->getContent(), RegisterWalkIn::class, 'json');
        $command->patientId = $patientId;


        $this->get('command_bus')->handle($command);

        return new Response(null, Response::HTTP_CREATED);
    }
}

<<< EOD
{
  "fistName": "",
  "lastName": "",
  "birthDate": "",
  "sex": "",
  "indication": ""
}
EOD;
