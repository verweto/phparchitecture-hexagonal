<?php

namespace Derp\Bundle\ERBundle\Command;

use Derp\Command\RegisterWalkIn;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RegisterWalkInConsoleCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('derp:register:walk-in');
        $this->addArgument('firstName');
        $this->addArgument('lastName');
        $this->addArgument('sex');
        $this->addArgument('birthDate');
        $this->addArgument('indication');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $command = new RegisterWalkIn();
        $command->firstName = $input->getArgument('firstName');
        $command->lastName = $input->getArgument('lastName');
        $command->sex = $input->getArgument('sex');
        $command->birthDate = \DateTime::createFromFormat('Y-m-d', $input->getArgument('birthDate'));
        $command->indication = $input->getArgument('indication');

        $validationList = $this->getContainer()->get('validator')->validate($command);
        if (count($validationList)) {
            echo $validationList;
            return 1;
        }

        $this->getContainer()->get('command_bus')->handle($command);
    }
}
