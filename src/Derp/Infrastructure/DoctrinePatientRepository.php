<?php

namespace Derp\Infrastructure;

use Derp\Bundle\ERBundle\Entity\Patient;
use Derp\Bundle\ERBundle\Entity\PatientId;
use Derp\Domain\PatientNotFound;
use Derp\Domain\PatientRepository;
use Rhumsaa\Uuid\Uuid;
use Symfony\Bridge\Doctrine\ManagerRegistry;

class DoctrinePatientRepository implements PatientRepository
{

    /**
     * @var ManagerRegistry
     */
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * @param Patient $patient
     */
    public function add(Patient $patient)
    {
        $em = $this->doctrine->getManager();
        $em->persist($patient);
    }

    /**
     * @inheritdoc
     */
    public function all()
    {
        return $this->doctrine->getManager()->getRepository(Patient::class)->findAll();
    }

    /**
     * @param string $lastName
     *
     * @return Patient[]
     */
    public function byLastName($lastName)
    {
        return $this
            ->doctrine
            ->getManager()
            ->getRepository(Patient::class)
            ->findBy(
                ['personalInformation.name.lastName' => $lastName]
            );
    }

    /**
     * @inheritdoc
     */
    public function byId($id)
    {
        $patient = $this
            ->doctrine
            ->getManager()
            ->getRepository(Patient::class)
            ->find($id);

        if ($patient ===  null) {
            throw new PatientNotFound();
        }

        return $patient;
    }

    /**
     * @return \Derp\Bundle\ERBundle\Entity\PatientId
     */
    public function nextIdentity()
    {
        return PatientId::fromString(Uuid::uuid4()->toString());
    }
}