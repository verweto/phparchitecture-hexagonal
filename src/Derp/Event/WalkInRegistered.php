<?php

namespace Derp\Event;

use Derp\Bundle\ERBundle\Entity\PatientId;
use SimpleBus\Message\Message;

class WalkInRegistered implements Message
{

    /**
     * @var PatientId
     */
    private $patientId;

    public function __construct(PatientId $patientId)
    {
        $this->patientId = $patientId;
    }

    /**
     * @return PatientId
     */
    public function getPatientId()
    {
        return $this->patientId;
    }
}

