<?php


namespace Derp\Event;


use Derp\Domain\PatientRepository;
use SimpleBus\Message\Message;
use SimpleBus\Message\Subscriber\MessageSubscriber;

class WhenWalkinRegisteredNotifyTriagNurse implements MessageSubscriber
{

    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var PatientRepository
     */
    private $repository;

    public function __construct(PatientRepository $repository, \Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
        $this->repository = $repository;
    }

    public function notify(Message $message)
    {
        /** @var WalkInRegistered $message */
        $patient = $this->repository->byId($message->getPatientId());

        /** @var WalkInRegistered $message */
        $mailMessage = \Swift_Message::newInstance(
            'A new patient has walked in',
            'Indication: ' . $patient->getIndication()
        );
        $mailMessage->setTo('triage-nurse@derp.nl');
        $this->mailer->send($mailMessage);
    }
}