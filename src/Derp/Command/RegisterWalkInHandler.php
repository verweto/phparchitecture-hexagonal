<?php

namespace Derp\Command;

use Derp\Bundle\ERBundle\Entity\BirthDate;
use Derp\Bundle\ERBundle\Entity\FullName;
use Derp\Bundle\ERBundle\Entity\Patient;
use Derp\Bundle\ERBundle\Entity\PersonalInformation;
use Derp\Bundle\ERBundle\Entity\Sex;
use Derp\Domain\PatientRepository;
use SimpleBus\Message\Handler\MessageHandler;
use SimpleBus\Message\Message;
use Symfony\Bridge\Doctrine\ManagerRegistry;

class RegisterWalkInHandler implements MessageHandler
{

    /**
     * @var PatientRepository
     */
    private $repository;

    public function __construct(
        PatientRepository $repository
    )
    {
        $this->repository = $repository;
    }

    public function handle(Message $command)
    {
        /** @var RegisterWalkIn $command */

        $patient = Patient::walkIn(
            $command->patientId,
            PersonalInformation::fromDetails(
                FullName::fromParts($command->firstName, $command->lastName),
                BirthDate::fromYearMonthDayFormat(
                    $command->birthDate->format('Y-m-d')
                ),
                new Sex($command->sex)
            ),
            $command->indication
        );

        $this->repository->add($patient);
    }
}
